from selenium import webdriver
from selenium.webdriver.opera.options import Options
import time

opdriver = '/home/mayank/apps/web_drivers/operadriver'

options = Options()
options.add_argument("--private")
try:
    driver = webdriver.Opera(executable_path=opdriver, options=options)
    driver.get('http://127.0.0.1:5000')
    print(driver.find_element_by_tag_name("title").text, ":", driver.title)
    time.sleep(5) # to view the result
except Exception as e:
    print(e)
finally:
    driver.quit()
