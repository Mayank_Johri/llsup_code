from selenium import webdriver
from selenium.webdriver import ActionChains
from time import sleep


try:
    driver = webdriver.Chrome()
    driver.get("http://127.0.0.1:5000/bulma")
    sleep(2)
    result = driver.execute_script("return country")
    print(result)
except Exception as e:
    print(e)
finally:    
    driver.quit()
