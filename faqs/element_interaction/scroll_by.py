### For Chome
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains


driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")
submit_button = driver.find_element_by_css_selector("button[class='button is-link']")

actions = ActionChains(driver)
actions.move_to_element(submit_button)
actions.perform()

driver.quit()
