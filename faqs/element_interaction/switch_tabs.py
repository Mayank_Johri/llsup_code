from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import sys

firefox_path = '/home/mayank/apps/firefox/firefox'
gecko_path = "/home/mayank/apps/web_drivers/geckodriver"

# options = Options()
binary = FirefoxBinary(firefox_path,
                       log_file=sys.stdout)

driver = webdriver.Firefox(executable_path=gecko_path,
                           firefox_binary=binary)
try:
	driver.get("http://127.0.0.1:5000/bulma")
	driver.execute_script('''window.open("http://127.0.0.1:5000/success", "_blank");''')
	driver.execute_script('''window.open("http://127.0.0.1:5000/form_1", "_blank");''')
	# driver.window_handles
	sleep(5)
	driver.find_element_by_tag_name('html').send_keys(Keys.CONTROL + Keys.TAB)
	sleep(3)
	driver.find_element_by_tag_name('html').send_keys(Keys.CONTROL + Keys.TAB)
	sleep(5)
	actions = ActionChains(driver)      
	actions.key_down(Keys.CONTROL).key_down(Keys.TAB).key_up(Keys.TAB).key_up(Keys.CONTROL).perform()
	sleep(5)
	driver.switch_to_window(driver.window_handles[2])
	sleep(5)
except Exception as e:
	print("Error: ", e)
finally:
	driver.quit()

