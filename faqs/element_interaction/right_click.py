from selenium import webdriver
from selenium.webdriver import ActionChains
from time import sleep


driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")

link = driver.find_element_by_css_selector("input[id='cb_test'] ~ a")
actions = ActionChains(driver)
actions.move_to_element(link)
actions.context_click(link)
actions.perform()
sleep(1)
driver.quit()
