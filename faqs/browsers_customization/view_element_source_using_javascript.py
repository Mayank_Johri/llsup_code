from selenium import webdriver

driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")
text_area = driver.find_element_by_css_selector("textarea[class='textarea']")
parent = driver.execute_script("return arguments[0].parentNode;",
                               text_area)
print(parent.get_attribute('innerHTML'))

driver.quit()
