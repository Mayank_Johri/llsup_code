"""
Getting console.log output from Chrome with Selenium Python API bindings
"""
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# enable browser logging
d = DesiredCapabilities.CHROME

d['loggingPrefs'] = {'browser': 'ALL'}
driver = webdriver.Chrome(desired_capabilities=d)

driver.get('http://127.0.0.1:5000/ramram')

for entry in driver.get_log('browser'):
    print(entry)
