### For Chome
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

chrome_options = Options()
chrome_options.add_argument("--headless")

driver = webdriver.Chrome(chrome_options=chrome_options)
driver.get("https://www.google.com")

search_taxt = driver.find_element_by_id("lst-ib")
search_button = driver.find_element_by_css_selector("input[value=\"Google Search\"]")

driver.close()
