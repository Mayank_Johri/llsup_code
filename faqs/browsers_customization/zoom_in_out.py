
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from time import sleep

chrome_options = Options()

driver = webdriver.Chrome(chrome_options=chrome_options)
driver.get("http://127.0.0.1:5000/bulma")
html = driver.find_element_by_tag_name("html")
print(html.__dict__)
times = 1.5
driver.execute_script("$('html').css('zoom', arguments[0]);", times)
sleep(3)
driver.execute_script("$('html').css('zoom', arguments[0]);", 0)
sleep(3)
driver.quit()
