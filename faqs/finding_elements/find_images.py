from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/imgs")

def image_status(driver, by, ident):
    result = driver.execute_script("""return arguments[0].complete 
                && typeof arguments[0].naturalWidth != \"undefined\" 
                &&  arguments[0].naturalWidth > 0""", driver.find_element(by, ident))
    return result

try:
    for a in ["a", "b", "c", "d"]:
        result = image_status(driver, By.ID, a)
        print(result)
except Exception as e:
    print(e)
finally:
    driver.quit()
