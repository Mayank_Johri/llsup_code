#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/cd1")

try:
    element = WebDriverWait(driver, 15).until(
        ec.text_to_be_present_in_element((By.ID, "timer"), "7")
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
