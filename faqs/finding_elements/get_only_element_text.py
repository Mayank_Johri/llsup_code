#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


def get_only_element_text(driver, element):
    return driver.execute_script("""
    return jQuery(arguments[0]).contents().filter(function() {
        return this.nodeType == Node.TEXT_NODE;
    }).text();
    """, element)


try:
    driver = webdriver.Chrome()
    driver.get("http://127.0.0.1:5000/nested")
    element = driver.find_element_by_id("parent")
    print(element.text)
    print("~^"*20)
    print(get_only_element_text(driver, element))
except Exception as e:
    print("Error:", e)
finally:
    driver.quit()
