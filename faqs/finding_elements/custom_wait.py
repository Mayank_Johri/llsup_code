from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By


class wait_for(object):
    def __init__(self, locator, text):
        self.locator = locator
        self.text = text

    def __call__(self, driver):
        try:
            element_text = EC._find_element(driver, self.locator).get_attribute("value")
            return element_text.startswith(self.text)
        except StaleElementReferenceException:
            print("Error")
            return False


driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/timeout")

WebDriverWait(driver,
              7).until(wait_for((By.CSS_SELECTOR,
                                 'input[id="name"]'),
                                "Mayank"))

driver.quit()
