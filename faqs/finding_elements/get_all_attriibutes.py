from selenium import webdriver
from selenium.webdriver.common.by import By

def get_properties(driver, element):
    val = driver.execute_script("""var items = {};
for (index = 0; index < arguments[0].attributes.length; ++index) {
items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value };
return items;""", element)
    return val
driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")

ele = driver.find_element(By.CSS_SELECTOR, "[type='email']")
print(get_properties(driver, ele))

driver.quit()
