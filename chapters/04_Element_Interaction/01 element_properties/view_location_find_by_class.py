#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("https://www.reddit.com/")
ele = driver.find_element_by_class_name("trending-subreddits-content")
print(ele.location)
