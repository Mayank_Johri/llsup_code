#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/")
ele = driver.find_element_by_id("pineapple")
print(ele.value_of_css_property("background-color"))
ele = driver.find_element_by_id("banana")
print(ele.value_of_css_property("color"))
