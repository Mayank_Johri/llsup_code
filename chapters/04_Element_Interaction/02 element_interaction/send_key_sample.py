#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/send_clear")

# click radio button
ele = driver.find_element_by_id("example-text-input")
ele.send_keys("TEST")
