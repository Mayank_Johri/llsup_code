#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_progress")

# click radio button
progresses = driver.find_elements_by_class_name("progress")

for prog in progresses:
    print(prog.find_element_by_class_name("progress-completed").text)
    print(prog.value_of_css_property("width"))
