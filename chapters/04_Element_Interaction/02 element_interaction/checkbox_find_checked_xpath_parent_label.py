#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://localhost:5000/form_cb_badge")

ele_s = driver.find_elements_by_xpath("//*[@id='primary']/parent::*")

for a in ele_s:
    print(a.text)
# WebElement parentElement = driver.findElement
# (By.xpath("//div[@id='gf-BIG']/parent::parentElementTag"));
