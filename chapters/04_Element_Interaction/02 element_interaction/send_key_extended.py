#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/send_clear")

# click radio button
data = {
    "example-text-input": "test",
    "example-search-input": "Mayank Johri",
    "example-email-input": "Mayank Johri",
    "example-tel-input": "Mayank Johri",
    "example-password-input": "test",
    "example-number-input": "tese",
    "example-number-input": "tese",
    "example-datetime-local-input": "Mayank",
    "example-date-input": "Mayank",
    "example-month-input": "Mayank",
    "example-week-input": "Mayank"
}

for key, val in data.items():
    ele = driver.find_element_by_id(key)
    ele.send_keys(val)
driver.save_screenshot("{filename}.png".format(filename=__file__))
