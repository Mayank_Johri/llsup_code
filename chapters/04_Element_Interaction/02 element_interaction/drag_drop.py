"""."""

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains


"""."""
options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(options=options)

driver.maximize_window()
driver.get('http://jqueryui.com/draggable/')

driver.switch_to.frame(0)

source1 = driver.find_element_by_id('draggable')
action = ActionChains(driver)

# move element by x,y coordinates on the screen
action.drag_and_drop_by_offset(source1, 200, 100).perform()
