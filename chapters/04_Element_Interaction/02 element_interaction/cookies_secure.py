"""interactive with secure cookies.

use `main_secure.py` as server
"""

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


handlSSLErr = DesiredCapabilities.OPERA
handlSSLErr["ACCEPT_SSL_CERTS"] = True

driver = webdriver.Opera(handlSSLErr)

driver.maximize_window()
driver.get('https://localhost:4040/cookies')

print(driver.get_cookie("nationalist"))
new_cookies = {'name': 'Visonary',
    'path': '/',
    'secure': True,
    'value': 'Avul Pakir Jainulabdeen Abdul Kalam'}
driver.add_cookie(new_cookies)
print("~" * 20)

for c in driver.get_cookies():
    print(c['name'], ":", c['value'])


driver.close()