#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def is_editable(element):
    """Still not working :(."""
    try:
        element.send_keys(Keys.DELETE)
        return True
    except Exception:
        return False


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/send_clear")

# click radio button
data = [
    "example-text-input",
    "example-search-input",
    "example-email-input",
    "example-tel-input",
    "example-password-input",
    "example-number-input",
    "example-number-input",
    "example-datetime-local-input",
    "example-date-input",
    "example-month-input",
    "example-week-input"
]

for idn in data:
    print(idn)
    element = driver.find_element_by_id(idn)
    if (is_editable(element)):
        try:
            element.clear()
        except Exception as e:
            print(e)

driver.save_screenshot("{filename}.png".format(filename=__file__))
