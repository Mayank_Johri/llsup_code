"""."""

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.opera.options import


driver = webdriver.Opera()

driver.maximize_window()
driver.get('http://localhost:5000/cookies')

print(driver.get_cookie("nationalist"))
new_cookies = {'name': 'Visonary',
    'path': '/',
    'secure': True,
    'value': 'Avul Pakir Jainulabdeen Abdul Kalam'}
driver.add_cookie(new_cookies)
print("~" * 20)

for c in driver.get_cookies():
    print(c['name'], ":", c['value'])

# driver.close()