#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/drag_and_drop")

des = driver.find_element_by_id("div1")
man = driver.find_element_by_id("drag1")
actions = ActionChains(driver)
# actions.move_to_element(bx)
actions.click_and_hold(man).move_to_element(des).release(des)
actions.click(des).pause(2).release()
actions.perform()
