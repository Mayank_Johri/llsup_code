#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("https://www.reddit.com/new")
reddit_img = driver.find_element_by_id("header-img")
reddit_img.click()
