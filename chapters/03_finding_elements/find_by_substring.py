#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


BIN = FirefoxBinary('/opt/firefox/firefox')
WD = webdriver.Firefox(firefox_binary=BIN)
WD.get("http://127.0.0.1:5000/substr")

new_post = WD.find_element_by_css_selector("[data-years|='1800']")
print(new_post.text)
