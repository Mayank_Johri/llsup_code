#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from utils import is_element_present


BIN = FirefoxBinary('/opt/firefox/firefox')
WD = webdriver.Firefox(firefox_binary=BIN)
WD.get("https://www.reddit.com/")

c = {
     "dob": "#desktop-onboarding-browse",
     "skip": ".skip-for-now",
     "username": "[name='user']",
     "password": "[name='passwd']"
    }

if is_element_present(WD, c["dob"]):
    WD.find_element_by_css_selector(c["skip"]).click()

username = WD.find_element_by_css_selector(c['username'])
password = WD.find_element_by_css_selector(c['password'])
username.send_keys("Roshan/")
password.send_keys("Good_PManager")
